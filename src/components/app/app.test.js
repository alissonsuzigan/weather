import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import App from './app';
import Header from '../header';
import Form from '../form';

describe('App component', () => {
  it('Should render at DOM without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('Should verify the child components', () => {
    const wrapper = shallow(<App />);
    expect(wrapper.contains(<Header />)).toBe(true);
    expect(wrapper.contains(<Form />)).toBe(true);
  });
});
