import { useReducer, createContext } from 'react';
import API from '../assets/scripts/api';

// Action types
const LOAD_WEATHER_REQUEST = 'LOAD_WEATHER_REQUEST';
const LOAD_WEATHER_SUCCESS = 'LOAD_WEATHER_SUCCESS';
const LOAD_WEATHER_ERROR = 'LOAD_WEATHER_ERROR';

// Action creators
export const showLoading = (city) => ({
  type: LOAD_WEATHER_REQUEST,
  payload: { city }
});

//
export const getWeatherByCity = async (city) => {
  try {
    const data = await API.getWeatherByCity(city);
    return {
      type: LOAD_WEATHER_SUCCESS,
      payload: { data, city }
    };
  }
  catch(error) {
    return {
      type: LOAD_WEATHER_ERROR,
      payload: { city }
    };
  }
};

// Initial application state
export const INITIAL_STATE = {
  loading: false,
  city: '',
  feedback: '',
  data: {}
}

// Reducer
export const reducer = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case LOAD_WEATHER_REQUEST:
      return {
        ...state,
        ...action.payload,
        loading: true,
        feedback: 'Loading...',
      };

    case LOAD_WEATHER_SUCCESS:
      const { data, city } = action.payload;
      return {
        ...state,
        city,
        data: { ...state.data, [data.name.toLowerCase()]: data },
        loading: false,
        feedback: ''
      };

    case LOAD_WEATHER_ERROR:
      return {
        ...state,
        ...action.payload,
        loading: false,
        feedback: 'City was not found.',
      };

    default:
      return state;
  }
}

export const userWeatherReducer = () => {
  return useReducer(reducer, INITIAL_STATE);
}

export default createContext(INITIAL_STATE);