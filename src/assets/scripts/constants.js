// API
// export default
export const IMAGE = {
  URL: 'https://api.unsplash.com/',
  SEARCH: 'search/photos/',
  RANDOM: 'photos/random/',
  KEY: '?client_id=d0579a104fa0b679930181110ef1a9f375d9257af3be134dae2679bac1adfba2',
  PER_PAGE: '&per_page=1&orientation=landscape',
  QUERY: '&query='
};

export const WEATHER = {
  // URL: 'https://api.openweathermap.org/data/2.5/forecast',
  URL: 'https://api.openweathermap.org/data/2.5/weather',
  KEY: '?APPID=24312b65699c3bccbcd0fab6eeaf5681',
  UNIT: '&units=metric',
  QUERY: '&q='
}

// https://samples.openweathermap.org/data/2.5/find?q=London&units=metric&appid=b6907d289e10d714a6e88b30761fae22

// export const WEATHER = {
//   URL: 'http://dataservice.accuweather.com/',
//   SEARCH: 'locations/v1/cities/search',
//   DAILY: 'forecasts/v1/daily/5day/',
//   KEY: '?apikey=BnbDlWiHxXWFtk7pOYwzAjhoNrAxl5Mp',
//   // KEY: '?apikey=BnbDlWiHxXWFtk7pOYwzAjhoNrAxl5Mp&language=pt-br',
// }

/*

https://vortex.accuweather.com/adc2010/images/slate/icons/41.svg

http://dataservice.accuweather.com/locations/v1/cities/search

?apikey=BnbDlWiHxXWFtk7pOYwzAjhoNrAxl5Mp
http: //dataservice.accuweather.com/locations/v1/cities/search
http: //dataservice.accuweather.com/forecasts/v1/daily/5day/[city-id]
*/