/**
 * Abstraction to the fetch method
 * @param {String} url
 * @param {Object} params
 * @return {Promise}
 */
export const customFetch = async (url, params = {}) => {
  const response = await fetch(url, params);
  if (response.ok) return response.json();
  return Promise.reject(new Error(response.status))
};

/**
 * Get latitude and longitude from geolocation
 * @return {Promise}
 */
export const getLocation = async () => {
  const getPosition = (options = {}) => {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resolve, reject, options);
    });
  };

  try {
    const position = await getPosition();
    const { latitude, longitude } = position.coords;
    return { latitude, longitude };
  } catch (error) {
    return Promise.reject(new Error(error))
  }
};