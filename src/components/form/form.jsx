import React, { useState, useContext, useRef } from 'react';
import { MdSearch, MdClose } from "react-icons/md";
// Store
import WeatherContext, { showLoading, getWeatherByCity } from '../../store/weather';

const Form = () => {
  const { state, dispatch } = useContext(WeatherContext);
  const [term, setTerm] = useState('');
  const [showModal, setShowModal] = useState(false);
  const inputEl = useRef(null);

  const loadWeather = async () => {
    if (term && term !== state.city) {
      dispatch(showLoading(term));
      const payload = await getWeatherByCity(term);
      dispatch(payload);

      if (payload.type === 'LOAD_WEATHER_SUCCESS' ) {
        setShowModal(false);
      }
    }
  };

  const onChange = (event) => {
    setTerm(event.target.value);
  };

  const onToggleModal = () => {
    setShowModal(prev => {
      if (!prev) {
        setTimeout(() => {
          setTerm('');
          inputEl.current.focus();
        }, 0);
      }
      return !prev;
    });
  };

  const submitWeather = (event) => {
    event.preventDefault();
    loadWeather();
  };

  return (
    <div className="search-content">
      <button className="fixed-button" onClick={onToggleModal} title="Search for a city"><MdSearch /></button>
      {showModal &&
        <div className="modal">
          <form onSubmit={submitWeather} className="form">
            <button className="close" title="Close modal"><MdClose size={24} onClick={onToggleModal} /></button>
            <label className="label" htmlFor="locale">City: </label>
            <input className="input" ref={inputEl} id="locale" value={term} onChange={onChange} placeholder="Search for a city" />
            <button className="submit" title="Search"><MdSearch size={24} /></button>
            <span className="feedback">{state.feedback} </span>
          </form>
        </div>
      }
    </div>
  );
};

export default Form;
