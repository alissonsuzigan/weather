import React from 'react';

const Card = ({ data }) => {
  const { name, dt, main, weather } = data;
  const [weatherInfo] = weather;
  const period = weatherInfo.icon.includes('d') ? 'day' : 'night'
  const icon = `card-icon wi wi-owm-${period}-${weatherInfo.id}`;
  const date = new Date(dt * 1000).toDateString();

  return (
    <section className="card">
      <header className="card-header">
        <h1 className="card-city">{name}</h1>
        <time className="card-date">{date}</time>
      </header>

      <div className="card-content">
        <div>
          <i className={icon}></i>
          <span className="card-sky">{weather[0].main}</span>
        </div>

        <h2 className="card-temp">{parseFloat(main.temp.toFixed(1))} °C</h2>

        <div className="card-max-min">
          <span>Min: {Math.round(main.temp_max)} °C</span>
          <span>Max: {Math.round(main.temp_min)} °C</span>
        </div>
      </div>
    </section>
  );
};

export default Card;
