import React from 'react';
import logo from '../../assets/images/logo.svg';

const Header = () => {
  return (
    <header className="header">
      <img src={logo} className="logo" alt="Weather" /><h1>Weather :: v{React.version}</h1>
    </header>
  );
};

export default Header;