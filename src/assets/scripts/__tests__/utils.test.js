import { customFetch, getLocation } from '../utils';
import { WEATHER } from '../constants';

describe('Utils scripts', () => {
  describe('customFetch method', () => {
    it('should validate success', async () => {
      const url = `${WEATHER.URL}${WEATHER.KEY}${WEATHER.QUERY}berlin`
      const result = await customFetch(url);
      expect(result).toBeInstanceOf(Object);
      expect(result.name).toBe('Berlin');
    });

    it('should validate error', async () => {
      const url = `${WEATHER.URL}${WEATHER.KEY}${WEATHER.QUERY}xxxxx`
      try {
        await customFetch(url);
      } catch (error) {
        expect(error).toEqual(Error(404));
      }
    });
  });

  describe('getLocation method', () => {
    it('should validate success', async () => {
      // const position = { coords: { latitude: 123, longitude: 123 } };
      // global.navigator.geolocation = {
      //   getCurrentPosition: jest.fn(async() => await position),
      //   watchPosition: jest.fn()
      // };

      try {
        const coords = await getLocation();
        expect(coords).toBeInstanceOf(Object);
      } catch (error) {
        expect(error).toEqual(Error('TypeError: Cannot read property \'getCurrentPosition\' of undefined'));
      }
    });
  });
});

