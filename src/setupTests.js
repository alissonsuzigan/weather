// import React from 'react';
// import { shallow, mount, render, configure } from 'enzyme';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

// Setup enzyme's react adapter
configure({ adapter: new Adapter() });

// global.React = React;
// global.shallow = shallow;
// global.mount = mount;
// global.render = render;

// const localStorageMock = {
//   getItem: jest.fn(),
//   setItem: jest.fn(),
//   clear: jest.fn(),
// };
// global.localStorage = localStorageMock;
