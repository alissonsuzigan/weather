import React, { useEffect } from 'react';
// Components
import Form from '../form';
import CardList from '../card';
// Store
import WeatherContext, { showLoading, getWeatherByCity, userWeatherReducer } from '../../store/weather';

const App = () => {
  const [state, dispatch] = userWeatherReducer();
  const cityList = ['waltham', 'berlin', 'barueri'];

  const loadWeatherByCityList = async () => {
    for (const city of cityList) {
      dispatch(showLoading());
      const data = await getWeatherByCity(city);
      dispatch(data);
    }
  };

  useEffect(() => {
    loadWeatherByCityList();
  }, []);

  return (
    <WeatherContext.Provider value={{ state, dispatch }}>
      <CardList/>
      <Form />
    </WeatherContext.Provider>
  );
};

export default App;
