import API from '../api';

describe('API scripts', () => {

  describe('getWeatherByCity method', () => {
    it('should throw an error', () => {
      expect(() => API.getWeatherByCity()).toThrow(Error);
    });

    it('should return an object with prop Key', async () => {
      const result = await API.getWeatherByCity('berlin');
      expect(result).toBeInstanceOf(Object);
      expect(result.name).toBe('Berlin');
    });
  });

  // describe('getDailyWeather method', () => {
  //   it('should throw an error', () => {
  //     expect(() => API.getDailyWeather()).toThrow(Error);
  //   });
  //   it('should throw an error', async () => {
  //     const result = await API.getDailyWeather('45853');
  //     expect(result[0]).toBeInstanceOf(Object);
  //     // expect(result[0].Key).toBe('45853');
  //     console.log('====', result);
  //   });
  // });

});
