import React, { useContext } from 'react';
// Component
import Card from './card';
// Store
import WeatherContext from '../../store/weather';

const CardList = () => {
  const { data } = useContext(WeatherContext).state;
  const cityList = [];

  for (const key in data) {
    cityList.unshift(data[key]);
  }

  return (
    <div className="card-list">
      {cityList.map(city => <Card key={city.name} data={city} />)}
    </div>
  );
};

export default CardList;