import React from 'react';
import { mount } from 'enzyme';
import Form from './form';

describe('Form component', () => {
  it('Should verify the component structure', () => {
    const wrapper = mount(<Form />);
    expect(wrapper.find('form.form')).toBeDefined();
    expect(wrapper.find('label').prop('htmlFor')).toBe('locale');
    expect(wrapper.find('input')).toHaveLength(2);
    expect(wrapper.find('input').first().prop('id')).toBe('locale');
    expect(wrapper.find('input').last().prop('type')).toBe('submit');
    expect(wrapper.find('p.msg')).toBeDefined();
  });

  it('Should validate state changes', () => {
    const wrapper = mount(<Form />);
    const input = wrapper.find('input').first();
    input.simulate('change', {target: {value: 'berlin'}});
    expect(wrapper.find('input').first().prop('value')).toBe('berlin');
  });

  it('Should validate submit method', () => {
    const wrapper = mount(<Form />);
    const input = wrapper.find('input').first();
    input.simulate('change', {target: {value: 'berlin'}});
    wrapper.find('form').simulate('submit');
  });

});
