import { WEATHER } from './constants';
import { customFetch } from './utils';

// API endpoints
const ENDPOINTS = {
  // IMAGE_RANDOM: `${IMAGE.URL}${IMAGE.RANDOM}${IMAGE.KEY}${IMAGE.PER_PAGE}${IMAGE.QUERY}weather-no-people`,
  // IMAGE_QUERY: `${IMAGE.URL}${IMAGE.SEARCH}${IMAGE.KEY}${IMAGE.PER_PAGE}${IMAGE.QUERY}`,
  WEATHER_SEARCH: (city) => `${WEATHER.URL}${WEATHER.KEY}${WEATHER.UNIT}${WEATHER.QUERY}${city}`,
  // WEATHER_DAILY: (cityId) => `${WEATHER.URL}${WEATHER.DAILY}${cityId}${WEATHER.KEY}`
};

// The core API module
export default {
  getWeatherByCity: (city) => {
    if (!city) throw Error('Param [city] was not defined!');
    const url = ENDPOINTS.WEATHER_SEARCH(city);
    return customFetch(url);
  },

  // getDailyWeather: (cityId) => {
  //   if (!cityId) throw Error('Param [cityId] was not defined!');
  //   const url = ENDPOINTS.WEATHER_DAILY(cityId);
  //   return customFetch(url);
  // },

  /**
   * Get image data from random endpoint
   * @return {Promise}
   */
  // getRandomImage: () => {
  //   const url = `${ENDPOINTS.IMAGE_RANDOM}`;
  //   return customFetch(url);
  // },

  /**
   * Get image data by city
   * @param {String} query
   * @return {Promise}
   */
  // getImageBycity: (city) => {
  //   const url = `${ENDPOINTS.IMAGE_QUERY}${city}`;
  //   return customFetch(url);
  // }
}