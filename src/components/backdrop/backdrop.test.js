import React from 'react';
import { shallow } from 'enzyme';
import Backdrop from './backdrop';

describe('Backdrop component', () => {
  it('Should verify the component structure', () => {
    const wrapper = shallow(<Backdrop />);
    expect(wrapper.find('figure.backdrop')).toBeDefined();
  });
});